Certbot role
=========

That role can help you to add let's encrypt certificate for your web server.

Role Variables
--------------

Need to add domains to "certbot_certs", and strongly recommend that you fill out email correctly in "certbot_admin_email" 

Dependencies
------------

"Standalone" method has no dependencies.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - certbot
